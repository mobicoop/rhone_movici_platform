export default {
  // Uncomment to enable dark theme
  // dark: true,
  options: {
    customProperties: true
  },
  themes: {
    light: {
      primary: "#0096DE",
      secondary: "#0096DE",
      tertiary: "#353D4D",
      accent: "#EA6619",
      error: "#D32F2F",
      info: "#2196F3",
      success: "#56bc5e",
      warning: "#EA6619"
    },
    dark: {
      primary: "#0096DE",
      secondary: "#0096DE",
      tertiary: "#353D4D",
      accent: "#EA6619",
      error: "#D32F2F",
      info: "#2196F3",
      success: "#56bc5e",
      warning: "#EA6619"
    }
  }
}