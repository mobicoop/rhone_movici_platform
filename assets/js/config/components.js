// BASE
import MHeader from '@clientComponents/base/MHeader'
import MFooter from '@clientComponents/base/MFooter'
import Home from '@clientComponents/home/Home'

export default {
  MHeader,
  MFooter,
  Home
}