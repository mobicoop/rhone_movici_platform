export default {
  fr: {
    title: "L’actu mov’ici",
    linkedin: {
      main: "Suivez toute l'actualité de Mov'ici",
      secondary: "Auvergne-Rhône-Alpes sur Linkedin"
    },
    discover: {
      main: "Toute l'actualité du covoiturage en Auvergne-Rhône-Alpes",
      secondary: "est sur LinkedIn"
    },
    card: {
      title: "Tirage au sort",
      content: "Le 18 septembre 2019, c'était la 5ème journée nationale de la qualité de l'air et à cette occasion, mov'ici a réalisé un tirage au sort la semaine du 16a...",
      more: "En savoir plus"
    },
    card1: {
      title: "Covoit'go",
      content: "Covoit'go, le nouveau service de covoiturage local et spontanné du Grand Chambéry opéré par ecov_fr.",
      more: "En savoir plus"
    }
  }
}