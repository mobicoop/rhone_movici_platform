

blueDark:=$(shell tput setaf 21)
blue:=$(shell tput setaf 33)

ifeq ($(shell uname),Darwin)
  os=darwin
else
  os=linux
endif

install:
	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache ;\

	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache;\

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)rhone_movici_platform ($(os)): Installing node deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker-compose -f docker-compose-builder-$(os).yml run --rm install
	@make -s install-vendor

install-vendor:

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)rhone_movici_platform ($(os)): Installing php deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker-compose -f docker-compose-builder-$(os).yml run --rm install-vendor


fixtures:
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)rhone_movici_platform ($(os)): Generating fixtures...)
	$(info $(blue)------------------------------------------------------$(reset))
	@docker-compose -f docker-compose-builder-$(os).yml run --rm fixtures

start:
	$(info rhone_movici_platform ($(os)): Starting rhone_movici_platform environment containers.)
	@docker-compose -f docker-compose-$(os).yml up -d
 
stop:
	$(info rhone_movici_platform ($(os)): Stopping rhone_movici_platform environment containers.)
	@docker-compose -f docker-compose-$(os).yml stop 

status:
	@docker ps -a | grep rhone_movici_platform_platform
	@docker ps -a | grep rhone_movici_platform_db
 
restart:
	$(info rhone_movici_platform ($(os)): Restarting rhone_movici_platform environment containers.)
	@make -s stop
	@make -s start

reload:
	$(info Make ($(os)): Restarting rhone_movici_platform environment containers.)
	@make -s stop
	@make -s remove
	@make -s start

remove:
	$(info rhone_movici_platform ($(os)): Stopping rhone_movici_platform environment containers.)
	@docker-compose -f docker-compose-$(os).yml down -v 
 
clean:
	@make -s stop
	@make -s remove
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)Drop all deps + containers + volumes)
	$(info $(blue)------------------------------------------------------$(reset))
	sudo rm -rf node_modules vendor

logs: 
	$(info $(blueDark)------------------------------------------------------)
	$(info $(blueDark)rhone_movici_platform+DB Logs)
	$(info $(blueDark)------------------------------------------------------$(reset))
	@docker logs -f rhone_movici_platform

go-platform:
	@docker exec -it rhone_movici_platform zsh